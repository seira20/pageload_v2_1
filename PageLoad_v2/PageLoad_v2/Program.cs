﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Xml;

namespace PageLoad_v2
{
    public class Program
    {
        private const string Usage = "Usage: PageLoad.exe [Please select: 1 = Firefox 2 = Chrome 3 = IE 4 = Edge] [BuildNo] [File Path for URL List] [Environment: Please indicate if local or remote (Example: Remote-10.112.151.20:5566)]";

        private static IWebDriver driver;

        static void Main(string[] args)
        {
            string Browser = null;
            string DateToday = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).ToString("dd-MMM-yyyy");
            string input = string.Empty;
            string input2 = string.Empty;
            string input3 = string.Empty;
            string input4 = string.Empty;

            if (args.Length <= 3)
            {
                Console.WriteLine(Usage);
            }
            else
            {
                input = args[0];
                if (input.Equals("1"))
                {
                    Browser = "Firefox";
                }
                else if (input.Equals("2"))
                {
                    Browser = "Chrome";
                }
                else if (input.Equals("3"))
                {
                    Browser = "IE";
                }
                else if (input.Equals("4"))
                {
                    Browser = "Edge";
                }

                input2 = args[1] + "_" + DateToday;
                input3 = args[2];
                input4 = args[3];
            }

            if (input.Equals("All"))
            {
                Console.WriteLine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory));
                XmlDocument xml = new XmlDocument();
                xml.Load(input3);

                XmlNodeList Url_List = xml.SelectNodes("url_list/url");

                string[] All_Browser = { "Firefox", "Chrome", "IE", "Edge" };

                foreach (string AllBrowser in All_Browser)
                {
                    foreach (XmlNode value in Url_List)
                    {
                        string cn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                        SqlConnection myConnection = new SqlConnection(cn);
                        myConnection.Open();

                        List<double> TempPageLoadTime = new List<double>();
                        List<double> TempDomCompleteTime = new List<double>();

                        string StartTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        for (int x = 0; x < 3; x++)
                        {
                            double LoadEventEnd = 0;
                            double ResponseEnd = 0;
                            double DomLoadComplete = 0;
                            double DomLoadEnd = 0;
                            double PageLoadTime = 0;
                            double DomComplete = 0;

                            if (AllBrowser.Equals("Chrome"))
                            {
                                if (input4.Equals("local"))
                                {
                                    driver = new ChromeDriver();
                                }
                                else
                                {
                                    ChromeOptions options = new ChromeOptions();
                                    options.AddArguments("start-maximized");
                                    driver = new RemoteWebDriver(new Uri("http://" + input4.Split('-')[1] + "/wd/hub"), options);
                                }

                                driver.Manage().Cookies.DeleteAllCookies();
                                driver.Navigate().GoToUrl(value.InnerText);
                            }
                            else if (AllBrowser.Equals("Firefox"))
                            {
                                if (input4.Equals("local"))
                                {
                                    FirefoxOptions options = new FirefoxOptions();
                                    options.AcceptInsecureCertificates = true;
                                    driver = new FirefoxDriver(options);
                                }
                                else
                                {
                                    FirefoxOptions options = new FirefoxOptions();
                                    options.AcceptInsecureCertificates = true;
                                    driver = new RemoteWebDriver(new Uri("http://" + input4.Split('-')[1] + "/wd/hub"), options);
                                }
                                driver.Manage().Cookies.DeleteAllCookies();
                                driver.Navigate().GoToUrl(value.InnerText);
                            }
                            else if (AllBrowser.Equals("Edge"))
                            {
                                if (input4.Equals("local"))
                                {
                                    driver = new EdgeDriver();
                                }
                                else
                                {
                                    EdgeOptions options = new EdgeOptions();
                                    //options.PageLoadStrategy = PageLoadStrategy.Normal;
                                    driver = new RemoteWebDriver(new Uri("http://" + input4.Split('-')[1] + "/wd/hub"), options);
                                }
                                driver.Manage().Cookies.DeleteAllCookies();
                                driver.Navigate().GoToUrl(value.InnerText);
                            }
                            else if (AllBrowser.Equals("IE"))
                            {
                                if (input4.Equals("local"))
                                {
                                    driver = new InternetExplorerDriver();
                                }
                                else
                                {
                                    InternetExplorerOptions options = new InternetExplorerOptions();
                                    //options.EnsureCleanSession = true;
                                    options.IgnoreZoomLevel = true;
                                    options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                                    //options.PageLoadStrategy = PageLoadStrategy.Normal;
                                    driver = new RemoteWebDriver(new Uri("http://" + input4.Split('-')[1] + "/wd/hub"), options);
                                }
                                driver.Manage().Cookies.DeleteAllCookies();
                                driver.Navigate().GoToUrl(value.InnerText);
                            }
                            Thread.Sleep(6000);

                            const string scriptToExecute =
                            "var performance = window.performance || window.mozPerformance || window.msPerformance || window.webkitPerformance || {}; var timings = performance.timing || {}; return timings.toJSON();";

                            var webTiming = (Dictionary<string, object>)((IJavaScriptExecutor)driver)
                            .ExecuteScript(scriptToExecute);

                            LoadEventEnd = double.Parse(webTiming["loadEventEnd"].ToString());
                            ResponseEnd = double.Parse(webTiming["fetchStart"].ToString());
                            PageLoadTime = Math.Round(((LoadEventEnd - ResponseEnd) / 1000.0), 2);
                           
                            DomLoadComplete = double.Parse(webTiming["domContentLoadedEventEnd"].ToString());
                            DomLoadEnd = double.Parse(webTiming["navigationStart"].ToString());
                            DomComplete = Math.Round(((DomLoadComplete - DomLoadEnd) / 1000.0), 2);

                            TempPageLoadTime.Add(PageLoadTime);
                            TempDomCompleteTime.Add(DomComplete);

                            Thread.Sleep(3000);
                            driver.Quit();
                        }

                        string EndTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        SqlCommand myCommand = new SqlCommand("Insert into ebiz_page_load values ('"
                                                                + StartTime + "', '"
                                                                + EndTime + "', '"
                                                                + value.InnerText + "', '"
                                                                + TempPageLoadTime[0] + "', '"
                                                                + TempDomCompleteTime[0] + "', '"
                                                                + TempPageLoadTime[1] + "', '"
                                                                + TempDomCompleteTime[1] + "', '"
                                                                + TempPageLoadTime[2] + "', '"
                                                                + TempDomCompleteTime[2] + "','"
                                                                + input2 + "', '"
                                                                + null + "', '"
                                                                + AllBrowser + "')", myConnection);
                        myCommand.ExecuteNonQuery();
                        myConnection = null;

                        Console.WriteLine("Url: " + value.InnerText + "| Browser: " + AllBrowser + "| Build No.: " + input2 + "| Pageload 1: " + TempPageLoadTime[0] + "| DomComplete 1: " + TempDomCompleteTime[0] + "| Pageload 2: " + TempPageLoadTime[1] + "| DomComplete 2: " + TempDomCompleteTime[1] + "| Pageload 3: " + TempPageLoadTime[2] + "| DomComplete 3: " + TempDomCompleteTime[2]);
                    }
                }
            }
            else
            { 
                Pageload(Browser, input2, input3, input4);
            }
        }



        public static void Pageload(string Browser, string Buildno, string XmlLocation, string Environment)
        {
            //string node_address = "http://10.64.202.100:5566/wd/hub" /**ndatta**/;
            //string node_address = "http://localhost:5566/wd/hub" /**localhost**/;
            string node_address = "http://10.64.253.121:5567/wd/hub" /**VDI**/;

            Console.WriteLine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory));
            XmlDocument xml = new XmlDocument();
            xml.Load(XmlLocation);

            XmlNodeList Url_List = xml.SelectNodes("url_list/url");

            foreach (XmlNode value in Url_List)
            {
                string cn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                SqlConnection myConnection = new SqlConnection(cn);
                myConnection.Open();

                List<double> TempPageLoadTime = new List<double>();
                List<double> TempDomCompleteTime = new List<double>();

                string StartTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                for (int x = 0; x < 3; x++)
                {
                    //double LoadEventEnd = 0;
                    double ResponseEnd = 0;
                    //double DomLoadComplete = 0;
                    double DomLoadEnd = 0;
                    double PageLoadTime = 0;
                    double DomComplete = 0;

                    if (Browser.Equals("Chrome"))
                    {
                        if (Environment.Equals("local")) {
                            driver = new ChromeDriver();
                        }
                        else {
                            ChromeOptions options = new ChromeOptions();
                            options.AddArguments("start-maximized");
                            driver = new RemoteWebDriver(new Uri("http://" + Environment.Split('-')[1] + "/wd/hub"), options);
                        }
                        
                        driver.Manage().Cookies.DeleteAllCookies();
                        driver.Navigate().GoToUrl(value.InnerText);
                    }
                    else if (Browser.Equals("Firefox"))
                    {
                        if (Environment.Equals("local"))
                        {
                            FirefoxOptions options = new FirefoxOptions();
                            options.AcceptInsecureCertificates = true;
                            driver = new FirefoxDriver(options);
                        }
                        else
                        {
                            FirefoxOptions options = new FirefoxOptions();
                            options.AcceptInsecureCertificates = true;
                            driver = new RemoteWebDriver(new Uri("http://" + Environment.Split('-')[1] + "/wd/hub"), options);
                        }
                        driver.Manage().Cookies.DeleteAllCookies();
                        driver.Navigate().GoToUrl(value.InnerText);
                    }
                    else if (Browser.Equals("Edge"))
                    {
                        if (Environment.Equals("local"))
                        {
                            driver = new EdgeDriver();
                        }
                        else
                        {
                            EdgeOptions options = new EdgeOptions();
                            //options.PageLoadStrategy = PageLoadStrategy.Normal;
                            driver = new RemoteWebDriver(new Uri("http://" + Environment.Split('-')[1] + "/wd/hub"), options);
                        }
                        driver.Manage().Cookies.DeleteAllCookies();
                        driver.Navigate().GoToUrl(value.InnerText);
                    }
                    else if (Browser.Equals("IE"))
                    {
                        if (Environment.Equals("local"))
                        {
                            driver = new InternetExplorerDriver();
                        }
                        else { 
                            InternetExplorerOptions options = new InternetExplorerOptions();
                            options.EnsureCleanSession = true;
                            options.IgnoreZoomLevel = true;
                            options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                            //options.PageLoadStrategy = PageLoadStrategy.Normal;
                            driver = new RemoteWebDriver(new Uri("http://" + Environment.Split('-')[1] + "/wd/hub"), options);
                        }
                        driver.Manage().Cookies.DeleteAllCookies();
                        driver.Navigate().GoToUrl(value.InnerText);
                    }
                    Thread.Sleep(6000);

                    const string scriptToExecute =
                    "var performance = window.performance || window.mozPerformance || window.msPerformance || window.webkitPerformance || {}; var timings = performance.getEntriesByType('navigation')[0].loadEventStart || {}; return timings";

                    var LoadEventEnd = ((IJavaScriptExecutor)driver)
                    .ExecuteScript(scriptToExecute);

                    const string scriptToExecute1 =
                   "var performance = window.performance || window.mozPerformance || window.msPerformance || window.webkitPerformance || {}; var timings = performance.getEntriesByType('navigation')[0].domContentLoadedEventEnd || {}; return timings";

                    var DomLoadComplete = ((IJavaScriptExecutor)driver)
                    .ExecuteScript(scriptToExecute1);

                    //LoadEventEnd = double.Parse(webTiming["loadEventStart"].ToString());
                    //ResponseEnd = double.Parse(webTiming["fetchStart"].ToString());
                    PageLoadTime = Math.Round((double.Parse(LoadEventEnd.ToString()) / 1000.0), 2);
                   
                    //DomLoadComplete = double.Parse(webTiming["domContentLoadedEventEnd"].ToString());
                    //DomLoadEnd = double.Parse(webTiming["navigationStart"].ToString());
                    DomComplete = Math.Round((double.Parse(DomLoadComplete.ToString()) / 1000.0), 2);

                    TempPageLoadTime.Add(PageLoadTime);
                    TempDomCompleteTime.Add(DomComplete);

                    Thread.Sleep(3000);
                    driver.Quit();
                }

                string EndTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SqlCommand myCommand = new SqlCommand("Insert into ebiz_page_load values ('"
                                                        + StartTime + "', '"
                                                        + EndTime + "', '"
                                                        + value.InnerText + "', '"
                                                        + TempPageLoadTime[0] + "', '"
                                                        + TempDomCompleteTime[0] + "', '"
                                                        + TempPageLoadTime[1] + "', '"
                                                        + TempDomCompleteTime[1] + "', '"
                                                        + TempPageLoadTime[2] + "', '"
                                                        + TempDomCompleteTime[2] + "','"
                                                        + Buildno + "', '"
                                                        + null + "', '"
                                                        + Browser + "')", myConnection);
                myCommand.ExecuteNonQuery();
                myConnection = null;

                Console.WriteLine("Url: " + value.InnerText + "| Browser: " + Browser + "| Build No.: " + Buildno + "| Pageload 1: " + TempPageLoadTime[0] + "| DomComplete 1: " + TempDomCompleteTime[0] + "| Pageload 2: " + TempPageLoadTime[1] + "| DomComplete 2: " + TempDomCompleteTime[1] + "| Pageload 3: " + TempPageLoadTime[2] + "| DomComplete 3: " + TempDomCompleteTime[2]);
            }
        }
    }

}
